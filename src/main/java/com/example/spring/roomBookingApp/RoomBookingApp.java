package com.example.spring.roomBookingApp;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication
//@ComponentScan(basePackages = "com.example.spring.roomBookingApp.common")
public class RoomBookingApp {
    public static void main(String[] args) {
        SpringApplication.run(RoomBookingApp.class, args);
    }
}

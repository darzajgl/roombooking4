package com.example.spring.roomBookingApp.controller;

import com.example.spring.roomBookingApp.model.Guest;
import com.fasterxml.jackson.databind.util.ISO8601Utils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
public class GuestController {
    private List<Guest> listOfGuests;

    public GuestController() {
        listOfGuests = new ArrayList<>();
        listOfGuests.add(new Guest("Adam", "Sadam", true, "2000-01-01", "2001-01-01"));
//        listOfGuests.add(new Guest("Adam", "Sadam", true, LocalDate.parse("2000-01-01"), LocalDate.parse("2001-01-01"), (new Room(12, "blue", 22, true))));
        listOfGuests.add(new Guest("Andriy", "Shevchuk", false,"2002-02-02", "2002-02-22"));
//        listOfGuests.add(new Guest("Andriy", "Shevchuk", false, LocalDate.parse("2002-02-02"), LocalDate.parse("2002-02-22"), (new Room(13, "red", 23, false))));
    }

    SimpleDateFormat formatter = new SimpleDateFormat(("yyyy-MM-dd"));

    @RequestMapping("/guest")
    public String indexGet() {
        return "guest/index";
    }

    @RequestMapping(value = "/guestform", method = RequestMethod.GET)
    public ModelAndView show_guest_form(Model model) {
        return new ModelAndView("guest/guestform", "guest", new Guest());
    }

    @RequestMapping(value = "save_the_guest")
    public ModelAndView save_guest(@ModelAttribute(value = "guest") Guest guest) {
        if (guest.getGuestId() < 1) {
            guest.setGuestId((listOfGuests.get((listOfGuests.size() - 1)).getGuestId() + 1));
            listOfGuests.add(guest);
        } else {
            Guest guestTemp = getGuestById(guest.getGuestId());
            guestTemp.setName(guest.getName());
            guestTemp.setSurname(guest.getSurname());

            guestTemp.setGoldMember(guest.isGoldMember());
            guestTemp.setCheckInDate(guest.getCheckInDate());
            guestTemp.setCheckOutDate(guest.getCheckOutDate());
            guestTemp.setCheckOutDate(guest.getCheckOutDate());
//            guestTemp.setRoom(guest.getRoom());
        }
//          send  email
        return new ModelAndView("redirect:/viewguest");
    }

    private Guest getGuestById(@RequestParam int guestId) {
        return listOfGuests.stream().filter(f -> f.getGuestId() == guestId).findFirst().get();
    }

    @RequestMapping(value = "viewguest")
    public ModelAndView viewguest(Model model) {
        return new ModelAndView("guest/viewguest", "listOfGuests", listOfGuests);
    }

    @RequestMapping(value = "/goBack")
    public ModelAndView back(@ModelAttribute(value = "guest") Guest guest) {
        return new ModelAndView("redirect:/guestform");
    }

    @RequestMapping(value = "/deleteGuest", method = RequestMethod.POST)
    public ModelAndView delete(@RequestParam(value = "guest_id") String guest_id) {
        listOfGuests.remove(getGuestById(Integer.parseInt(guest_id)));
        return new ModelAndView("redirect:/viewguest");
    }
    @RequestMapping(value = "/edit_guest")
    public ModelAndView edit(@RequestParam(value = "guest_id") String guest_id) {
        Guest guestTemp = getGuestById(Integer.parseInt(guest_id));

        guestTemp.setName(guestTemp.getName());
        guestTemp.setGuestId(guestTemp.getGuestId());
        guestTemp.setSurname(guestTemp.getSurname());

        guestTemp.setGoldMember(guestTemp.isGoldMember());
        guestTemp.setCheckInDate(guestTemp.getCheckInDate());
        guestTemp.setCheckOutDate(guestTemp.getCheckOutDate());
        guestTemp.setCheckOutDate(guestTemp.getCheckOutDate());

        return new ModelAndView("guest/guestform", "guest", guestTemp);
    }

}
